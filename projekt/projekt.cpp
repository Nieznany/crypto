#include <gmpxx.h>

#include <iostream>
#include <utility>

gmp_randclass rng(gmp_randinit_default);

class EdwardsCurve;

class ModuloField {
 public:
  ModuloField(mpz_class value, mpz_class modulo)
      : value(value), modulo(modulo) {
    while (value < 0) {
      value += modulo;
    }
    value %= modulo;
  }
  bool operator==(const ModuloField &other) const {
    return value == other.value;
  }

  ModuloField operator+(const ModuloField &other) const {
    return ModuloField((value + other.value) % modulo, modulo);
  }

  ModuloField operator-(const ModuloField &other) const {
    return ModuloField((value - other.value + modulo) % modulo, modulo);
  }
  ModuloField operator-() const {
    return ModuloField((modulo - value) % modulo, modulo);
  }

  ModuloField operator*(const ModuloField &other) const {
    return ModuloField((value * other.value) % modulo, modulo);
  }

  ModuloField operator/(const ModuloField &other) const {
    mpz_class res;
    mpz_invert(res.get_mpz_t(), other.value.get_mpz_t(), modulo.get_mpz_t());
    return ModuloField((value * res) % modulo, modulo);
  }

  mpz_class getModulo() const { return modulo; }

 private:
  mpz_class value;
  mpz_class modulo;

  friend std::ostream &operator<<(std::ostream &os, const ModuloField &m);
};

class Point {
 public:
  Point(const ModuloField &x, const ModuloField &y, const ModuloField &d)
      : x(x), y(y), d(d) {}
  bool operator==(const Point &other) const {
    return x == other.x && y == other.y;
  }

  bool onCurve() const {
    auto modulo = x.getModulo();
    return x * x + y * y == ModuloField(1, modulo) + d * x * x * y * y;
  }

  Point operator+(const Point &other) const {
    auto modulo = x.getModulo();
    ModuloField one(1, modulo);
    ModuloField x1 = this->x;
    ModuloField y1 = this->y;
    ModuloField x2 = other.x;
    ModuloField y2 = other.y;
    ModuloField x3 = (x1 * y2 + y1 * x2) / (one + d * x1 * y1 * x2 * y2);
    ModuloField y3 = (y1 * y2 - x1 * x2) / (one - d * x1 * y1 * x2 * y2);
    return Point(x3, y3, d);
  }

  Point operator-(const Point &other) const {
    auto modulo = x.getModulo();
    ModuloField one(1, modulo);
    ModuloField x1 = this->x;
    ModuloField y1 = this->y;
    ModuloField x2 = -other.x;
    ModuloField y2 = other.y;
    ModuloField x3 = (x1 * y2 + y1 * x2) / (one + d * x1 * y1 * x2 * y2);
    ModuloField y3 = (y1 * y2 - x1 * x2) / (one - d * x1 * y1 * x2 * y2);
    return Point(x3, y3, d);
  }

  Point operator*(mpz_class exp) const {
    auto modulo = x.getModulo();
    Point r0(ModuloField(0, modulo), ModuloField(1, modulo), d);
    Point r1 = *this;
    size_t m = mpz_sizeinbase(exp.get_mpz_t(), 2);
    for (size_t i = 0; i < m; ++i) {
      int bit = mpz_tstbit(exp.get_mpz_t(), m - i - 1);
      if (bit == 1) {
        r0 = r0 + r1;
        r1 = r1 + r1;
      } else {
        r1 = r0 + r1;
        r0 = r0 + r0;
      }
    }
    return r0;
  }

 private:
  ModuloField d;
  ModuloField x;
  ModuloField y;

  friend std::ostream &operator<<(std::ostream &os, const Point &m);
};

class EdwardsCurve {
 public:
  EdwardsCurve(mpz_class modulo, mpz_class d, mpz_class p)
      : modulo(modulo), d(d, modulo), p(p) {}

  Point getBasePoint() const{
    mpz_class x(
        "2705691079882681090389589001251962954446177367541711474502428610129");
    mpz_class y = 28;
    return Point(ModuloField(x, modulo), ModuloField(y, modulo), d);
  }

  Point getZeroPoint() const{
    mpz_class x = 0;
    mpz_class y = 1;
    return Point(ModuloField(x, modulo), ModuloField(y, modulo), d);
  }

  mpz_class getKey() const{
    mpz_class k = 0;
    while (k <= 1) k = rng.get_z_range(p);
    return k;
  }

 private:
  mpz_class modulo;
  mpz_class p;
  ModuloField d;
};

std::ostream &operator<<(std::ostream &os, const ModuloField &m) {
  return os << m.value;
}

std::ostream &operator<<(std::ostream &os, const Point &m) {
  return os << m.x << " " << m.y;
}

std::ostream &operator<<(std::ostream &os, const std::pair<Point, Point> &m) {
  return os << m.first << " " << m.second;
}

class CurveE222 : public EdwardsCurve {
 public:
  CurveE222()
      : EdwardsCurve(
            mpz_class(
                "673998666678765994866675377175490766840928610563514312027"
                "5902562187"),
            mpz_class(160102),
            mpz_class(
                "168499666669691498716668844293872673556973745676005829418"
                "5521417407")) {}
};

template <typename Curve>
class Participant {
 private:
  Curve curve;
  int index;
  mpz_class x;
  mpz_class c;

 public:
  Participant(int index, bool voting) : index(index) {
    x = curve.getKey();
    c = x;
    if (voting) {
      while (c == x) {
        c = curve.getKey();
      }
    }
  }
  int getIndex() const { return index; }
  Point getGx() const { return curve.getBasePoint() * x; }
  Point getGcy(const std::vector<Participant>& participants) const {
    Point results = curve.getZeroPoint();
    for (auto& participant : participants) {
        if (participant.getIndex() < index) {
            results = results + participant.getGx();
        } else if (participant.getIndex() > index) {
            results = results - participant.getGx();
        }
    }

    results = results * c;
    return results;
  }
};

int main() {
  int n;
  std::cout << "Number of participants: ";
  std::cin >> n;
  std::vector<Participant<CurveE222>> participants;
  std::cout << "0 - no; 1 - yes\n";
  for (int i = 0; i < n; ++i) {
    std::cout << "Participant " << i << " vote: ";
    bool vote;
    std::cin >> vote;
    participants.emplace_back(i, vote);
  }
  for (auto &participant : participants) {
    std::cout << "Participant " << participant.getIndex() << " point g * [x] "
              << participant.getGx() << std::endl;
  }
  Point res = CurveE222().getZeroPoint();

  for (auto &participant : participants) {
    Point gcy = participant.getGcy(participants);
    res = res + gcy;
    std::cout << "Participant " << participant.getIndex() << " point g * [cy] "
              << gcy << std::endl;
  }

  std::cout << "Results: " << res << std::endl;
  if (res == CurveE222().getZeroPoint()) {
    std::cout << "Final vote: 0" << std::endl;
  }
  else {
    std::cout << "Final vote: 1" << std::endl;
  }

  // Curve E-222
}