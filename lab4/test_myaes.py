import pytest
from lab4.myaes import oracle, challange
from Crypto.Cipher import AES

@pytest.mark.parametrize('mode', [
    AES.MODE_CBC,
    AES.MODE_OFB,
])
def test_oracle_with_iv(mode):
    key = b'\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0A\x0B\x0C\x0D\x0E\x0F'
    plaintexts = [b'asdf', b'test', b'siala baba mak \x15']
    plaintexts = [msg.ljust(16, b'\x00') for msg in plaintexts]
    ivcipertexts = oracle(key, mode, plaintexts)

    for (iv, ciphertext), plaintexts in zip(ivcipertexts, plaintexts):
        crypto = AES.new(key, mode, iv)
        decrypted_plaintext = crypto.decrypt(ciphertext)

        assert decrypted_plaintext == plaintexts


@pytest.mark.parametrize('mode', [
    AES.MODE_CTR
])
def test_oracle_with_nonce(mode):
    key = b'\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0A\x0B\x0C\x0D\x0E\x0F'
    plaintexts = [b'asdf', b'test', b'siala baba mak \x15']
    plaintexts = [msg.ljust(16, b'\x00') for msg in plaintexts]
    ivcipertexts = oracle(key, mode, plaintexts)

    for (nonce, ciphertext), plaintexts in zip(ivcipertexts, plaintexts):
        crypto = AES.new(key, mode, nonce=nonce)
        decrypted_plaintext = crypto.decrypt(ciphertext)

        assert decrypted_plaintext == plaintexts


@pytest.mark.parametrize('mode', [
    AES.MODE_CBC,
    AES.MODE_OFB,
])
def test_challange_with_iv(mode):
    key = b'\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0A\x0B\x0C\x0D\x0E\x0F'
    plaintexts = [b'asdf', b'test']
    m1, m2 = [msg.ljust(16, b'\x00') for msg in plaintexts]
    b, iv, ciphertext = challange(key, mode, m1, m2)

    crypto = AES.new(key, mode, iv = iv)
    decrypted_plaintext = crypto.decrypt(ciphertext)

    if b == 0:
        assert decrypted_plaintext == m1
    else:
        assert decrypted_plaintext == m2



@pytest.mark.parametrize('mode', [
    AES.MODE_CTR
])
def test_challange_with_nonce(mode):
    key = b'\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0A\x0B\x0C\x0D\x0E\x0F'
    plaintexts = [b'asdf', b'test']
    m1, m2 = [msg.ljust(16, b'\x00') for msg in plaintexts]
    b, nonce, ciphertext = challange(key, mode, m1, m2)

    crypto = AES.new(key, mode, nonce = nonce)
    decrypted_plaintext = crypto.decrypt(ciphertext)
    if b == 0:
        assert decrypted_plaintext == m1
    else:
        assert decrypted_plaintext == m2
