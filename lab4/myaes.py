import argparse

import sys
from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes, random
import base64
import getpass
import jks

iv = bytearray(get_random_bytes(16))

NONCE_MODES = [AES.MODE_CTR]

def get_iv():
    global iv
    for i in range(16):
        iv[i] =(iv[i]+ 1)%256
        if not iv[i] == '\x00':
            break
    return bytes(iv)

def oracle(key, mode, messages):
    res = []
    for msg in messages:
        iv = get_iv()

        if mode in NONCE_MODES:
            iv = iv[:8]
            crypto = AES.new(key, mode, nonce=iv)
        else:
            crypto = AES.new(key, mode, iv=iv)
        ciphertext = crypto.encrypt(msg)
        res.append((iv, ciphertext))
    return res

def challange(key, mode, m1, m2):
    random_byte = random.randrange(2)
    if random_byte:
        m = m2
    else:
        m = m1
    iv = get_iv()

    if mode in NONCE_MODES:
        iv = iv[:8]
        crypto = AES.new(key, mode, nonce=iv)
    else:
        crypto = AES.new(key, mode, iv=iv)
    ciphertext = crypto.encrypt(m)
    return (random_byte, iv, ciphertext)

def get_key(path, id, quiet):
    prompt = '' if quiet else 'Password: '
    password = input(prompt)
    return jks.jks.KeyStore.load(path, password).entries[id].key


def oracle_step(key, mode, quiet):
    if not quiet:
        print('Provide number of messages:')
        print('>>> ', end='')
    q = int(input())
    messages = []
    for i in range(q):
        if not quiet:
            print('Provide value of msg in base64 format')
            print('>>> ', end='')
        b64msg = input()
        msg = base64.b64decode(b64msg).ljust(16, b'\x00')
        messages.append(msg)
    res = oracle(key, mode, messages)
    for iv, cyphertext in res:
        if not quiet:
            print('iv=', end='')
        print(base64.b64encode(iv).decode('utf-8'))
        if not quiet:
            print('cyphertext=', end='')
        print(base64.b64encode(cyphertext).decode('utf-8'))


def challange_step(key, mode, quiet):
    if not quiet:
        print('Provide value of msg in base64 format')
        print('>>> ', end='')
    b64msg = input()
    m1 = base64.b64decode(b64msg).ljust(16, b'\x00')

    if not quiet:
        print('Provide value of msg in base64 format')
        print('>>> ', end='')
    b64msg = input()
    m2 = base64.b64decode(b64msg).ljust(16, b'\x00')

    b, iv, cyphertext = challange(key, mode, m1, m2)
    if not quiet:
        print('iv=', end='')
    print(base64.b64encode(iv).decode('utf-8'))
    if not quiet:
        print('cyphertext=', end='')
    print(base64.b64encode(cyphertext).decode('utf-8'))

    if not quiet:
        print('Zgadnij b')
        print('>>> ', end='')

    b2=int(input())
    if b == b2:
        if not quiet:
            print('Sukces')
        else:
            print(1)
    else:
        if not quiet:
            print('Porażka')
        else:
            print(0)



def step(key, mode, quiet):
    if not quiet:
        print('o - Oracle')
        print('c - challange')
        print('e - exit')
        print('>>> ', end='')
    select = input()

    if select == 'o':
        oracle_step(key, mode, quiet)
    elif select == 'c':
        challange_step(key, mode, quiet)
    else:
        exit(0)



def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-e', '--encryption', choices=['CBC', 'OFB', 'CTR'], required=True)
    parser.add_argument('-p', '--path', required=True)
    parser.add_argument('-i', '--id', required=True)
    parser.add_argument('-q', '--quiet', action='store_true')

    args = parser.parse_args()

    if args.encryption == 'CBC':
        mode = AES.MODE_CBC
    elif args.encryption == 'OFB':
        mode = AES.MODE_OFB
    elif args.encryption == 'CTR':
        mode = AES.MODE_CTR

    key = get_key(args.path, args.id, args.quiet)

    while True:
        step(key, mode, args.quiet)


if __name__ == '__main__':
    main()