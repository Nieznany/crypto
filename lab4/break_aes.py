from subprocess import Popen, PIPE, STDOUT
import base64

def get_niv(iv):
    miv = bytearray(iv)
    for i in range(16):
        miv[i] = (miv[i] + 1) % 256
        if not miv[i] == '\x00':
            break
    iv = bytes(miv)
    return iv

def byte_xor(ba1, ba2):
    return bytes([_a ^ _b for _a, _b in zip(ba1, ba2)])


def main():
    p = Popen(['python', 'myaes.py', '-p' , 'ks.jck', '-i', 'keyid', '-e', 'CBC', '-q'], stdout=PIPE, stdin=PIPE)
    p.stdin.write(b'changeit\n')
    p.stdin.write(b'o\n1\n')
    plaintext=b'alamakota'.ljust(16, b'\x00')
    b64plaintext= base64.b64encode(plaintext)
    p.stdin.write(b64plaintext)
    p.stdin.write(b'\n')
    p.stdin.flush()
    iv = base64.b64decode(p.stdout.readline())
    ciphertext = base64.b64decode(p.stdout.readline())

    print('iv', iv)
    print('c', ciphertext)

    niv = get_niv(iv)

    print('niv', niv)

    plaintext1 = byte_xor(byte_xor(plaintext, iv), niv)
    b64plaintext1 = base64.b64encode(plaintext1)

    plaintext2=b'alamapsa'.ljust(16, b'\x00')
    b64plaintext2 = base64.b64encode(plaintext2)

    p.stdin.write(b'c\n')
    p.stdin.write(b64plaintext1)
    p.stdin.write(b'\n')
    p.stdin.write(b64plaintext2)
    p.stdin.write(b'\n')
    p.stdin.flush()

    iv2 = base64.b64decode(p.stdout.readline())
    ciphertext2 = base64.b64decode(p.stdout.readline())

    print('iv2', iv2)
    print('c2', ciphertext2)

    if ciphertext == ciphertext2:
        p.stdin.write(b'0\n')
    else:
        p.stdin.write(b'1\n')
    p.stdin.flush()
    res = p.stdout.readline()
    print(res)
    p.stdin.write(b'e\n')
    p.stdin.flush()
    p.wait()

if __name__ == '__main__':
    main()