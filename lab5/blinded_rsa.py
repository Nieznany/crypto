from itertools import count

from numpy import math
import random
from sympy import randprime

from scipy import stats
import numpy as np

def egcd(a, b):
    if a == 0:
        return (b, 0, 1)
    else:
        g, y, x = egcd(b % a, a)
        return(g, x - (b//a) *y, y )

def modinv(a, m):
    g, x, y = egcd(a, m)
    if g != 1:
        raise Exception('does not exist')
    else:
        return x % m

def GenModulus(w):
    n = len(w) // 2
    p = randprime(2**n, 2**(n+1))
    q = randprime(2**n, 2**(n+1))
    N = p * q
    return N, p, q

def GenRSA(w):
    n = len(w)
    N, p, q = GenModulus(w)
    m = (p-1) * (q-1)
    e = 2**16 + 1
    d = modinv(e, m)
    return N, e, d, p , q

def enc(x, N, e):
    return fast_pow(x, N, e)[0]

def dec(c, N, d, e):
    return blinded_fast_pow(c, N, d, e)[0]

def fast_pow(c, N, d):
    d_bin = "{0:b}".format(d)
    d_len = len(d_bin)
    reductions = 0
    h = 0
    x = c
    for j in range(1, d_len):
        x, r = mod_reduce(x ** 2, N)
        reductions = reductions + r
        if d_bin[j] == "1":
            x, r = mod_reduce(x * c, N)
            reductions = reductions + r
            h = h + 1
    return x, h, reductions

# t = A + r + R

def blinded_fast_pow(c, N, d, e):
    r = random.randrange(2, N)
    reductions = 0
    h = 0

    x, h_tmp, reductions_tmp = fast_pow(r, N, e) # x = r^e
    reductions += reductions_tmp
    h += h_tmp

    x *= c
    x, reductions_tmp = mod_reduce(x, N) # x= r^e * c
    reductions += reductions_tmp

    x, h_tmp, reductions_tmp = fast_pow(x, N, d) # x= r^(ed) * c^d = r* c^d
    reductions += reductions_tmp
    h += h_tmp

    r_inv = modinv(r, N)
    x *= r_inv
    x, reductions_tmp = mod_reduce(x, N)
    reductions += reductions_tmp

    return x, h, reductions


def mod_reduce(a, b):
    reductions = 0
    if a >= b:
        a = a % b
        reductions = 1
    return a, reductions

c_generator = None
M = 10000
def generate_c(N, w):
    global c_generator
    if c_generator is None:
        c_generator = count(M)
    for i in range(1000):
        yield next(c_generator)


def is_reduced(c, c_exp, N):
    c_exp = c_exp * c_exp % N
    if c_exp * c >= N:
        return True
    return False


def find_prefix(N, d):
    d_prefix = 1
    size = 1000
    for i in count(1):
        if math.floor(pow(N, 1/(d_prefix*2))) - math.ceil(pow(N, 1/(d_prefix*2+1))) < size or math.floor(pow(N, 1 / (d_prefix*2+1))) - 2 < size:
            return d_prefix, i

        c_reduced = [random.randint(math.ceil(pow(N, 1/(d_prefix*2+1))), math.floor(pow(N, 1/(d_prefix*2)))) for _ in range(100)]
        c_n_reduced = [random.randint(2, math.floor(pow(N, 1 / (d_prefix*2+1)))) for _ in range(100)]

        reduced_rs = []
        n_reduced_rs = []
        for c in c_reduced:
            x, h, r = blinded_fast_pow(c, N, d, e)
            x, h, r2 = fast_pow(c, N, d_prefix)
            reduced_rs.append(r - r2)

        for c in c_n_reduced:
            x, h, r = blinded_fast_pow(c, N, d, e)
            x, h, r2 = fast_pow(c, N, d_prefix)
            n_reduced_rs.append(r - r2)

        reduced_mean = np.mean(reduced_rs)
        n_reduced_mean = np.mean(n_reduced_rs)

        d_prefix *= 2
        if n_reduced_mean < reduced_mean - 0.2:
            d_prefix += 1
    return d_prefix



if __name__ == '__main__':

    w = 64
    N, e, d, p, q = GenRSA("0"*w)

    x = 6
    c = enc(x, N, e)

    print('x', x)
    print('c', c)
    print('dec(c)', dec(c, N, d, e))


    d_bin = "{0:b}".format(d)
    d_start, k = find_prefix(N, d)
    c_to_r = dict()
    cs = []
    for c in generate_c(N, w):
        x, h, r = blinded_fast_pow(c, N, d, e)
        x, h, r2 = fast_pow(c, N, d_start)
        c_to_r[c] = r - r2

        cs.append(c)


    c_c_exp_list = [(c, dec(c, N, d_start, e)) for c in cs]

    d_prefix = d_start
    print("{0:b}".format(d))
    print("{0:b}".format(d_prefix), end='')
    for _ in range(w - k + 2):
        reduced_rs = []
        n_reduced_rs = []
        for c, c_exp in c_c_exp_list:
            if is_reduced(c, c_exp, N):
                reduced_rs.append(c_to_r[c])
            else:
                n_reduced_rs.append(c_to_r[c])

        while len(n_reduced_rs) == 0 or len(reduced_rs) == 0:
            c = next(c_generator)

            x, h, r = blinded_fast_pow(c, N, d, e)
            x, h, r2 = fast_pow(c, N, d_start)
            c_to_r[c] = r - r2

            cs.append(c)
            c_exp, _, _ = fast_pow(c, N, d_prefix)
            if is_reduced(c, c_exp, N):
                reduced_rs.append(c_to_r[c])
            else:
                n_reduced_rs.append(c_to_r[c])
            c_c_exp_list.append((c, c_exp))



        reduced_mean = np.mean(reduced_rs)
        n_reduced_mean = np.mean(n_reduced_rs)


        d_prefix *= 2
        if n_reduced_mean < reduced_mean - 0.5:
            d_prefix += 1
            print('1', end='')
            c_c_exp_list = [(c, c *c_exp * c_exp % N) for c, c_exp in c_c_exp_list]
        else:
            print('0', end='')
            c_c_exp_list= [(c, c_exp*c_exp % N) for c, c_exp in c_c_exp_list]
    c = 5
    while  enc(dec(c,N,d_prefix, e), N, e) != c:
        d_prefix //= 2
        if d_prefix == 0:
            print("solution not found")
            exit(1)
    print()
    print("final solution")
    print("{0:b}".format(d_prefix))
    print("key")
    print("{0:b}".format(d))