from itertools import count

from numpy import math
import time
import random
from sympy import randprime
from scipy import stats
import numpy as np
import matplotlib.pyplot as plt

def egcd(a, b):
    if a == 0:
        return (b, 0, 1)
    else:
        g, y, x = egcd(b % a, a)
        return(g, x - (b//a) *y, y )

def modinv(a, m):
    g, x, y = egcd(a, m)
    if g != 1:
        raise Exception('does not exist')
    else:
        return x % m

def GenModulus(w):
    n = len(w) // 2
    p = randprime(2**n, 2**(n+1))
    q = randprime(2**n, 2**(n+1))
    N = p * q
    return N, p, q

def GenRSA(w):
    n = len(w)
    N, p, q = GenModulus(w)
    m = (p-1) * (q-1)
    e = 2**16 + 1
    d = modinv(e, m)
    return N, e, d, p , q

def enc(x, N, e):
    return fast_pow(x, N, e)[0]

def dec(c, N, d):
    return fast_pow(c, N, d)[0]

def fast_pow(c, N, d):
    d_bin = "{0:b}".format(d)
    d_len = len(d_bin)
    h = 0
    c = np.uint64(c)
    x = np.uint64(c)
    N = np.uint64(N)
    tab = [d_bit == "1" for d_bit in d_bin]
    start = time.time()
    for j in range(1, d_len):
        x *= x
        if x >= N:
            x %= N
        if tab[j]:
            x *= c
            if x >= N:
                x %= N
    return x, h, time.time() - start


def mod_reduce(a, b):
    reductions = 0
    if a >= b:
        a = a % b
        reductions = 1
    return a, reductions

c_generator = None
M = 10000
def generate_c(N, w):
    global c_generator
    if c_generator is None:
        c_generator = count(M)
    for i in range(1000):
        yield next(c_generator)


def is_reduced(c, c_exp, N):
    c_exp = c_exp * c_exp % N
    if c_exp * c >= N:
        return True
    return False


def find_prefix(N, d):
    d_prefix = 1
    size = 10
    print("{0:b}".format(d))

    for i in count(1):
        if math.floor(pow(N, 1/(d_prefix*2))) - math.ceil(pow(N, 1/(d_prefix*2+1))) < size or math.floor(pow(N, 1 / (d_prefix*2+1))) - 2 < size:
            return d_prefix, i

        c_reduced = [random.randint(math.ceil(pow(N, 1/(d_prefix*2+1))), math.floor(pow(N, 1/(d_prefix*2)))) for _ in range(1000000)]
        c_n_reduced = [random.randint(2, math.floor(pow(N, 1 / (d_prefix*2+1)))) for _ in range(1000000)]

        reduced_rs = []
        n_reduced_rs = []
        for c in c_reduced:
            x, h, r = fast_pow(c, N, d)
            reduced_rs.append(r)

        for c in c_n_reduced:
            x, h, r = fast_pow(c, N, d)
            n_reduced_rs.append(r)

        reduced_mean = np.mean(reduced_rs)
        n_reduced_mean = np.mean(n_reduced_rs)
        print(reduced_mean)
        print(n_reduced_mean)

        d_prefix *= 2
        if n_reduced_mean < reduced_mean - 0.05e-05:
            d_prefix += 1
            print(1)
        else:
            print(0)

        plt.hist(reduced_rs, bins=100, alpha=0.5, label='reduced')
        plt.hist(n_reduced_rs, bins=100, alpha=0.5, label='not reduced')
        plt.legend()
        plt.show()
    return d_prefix



if __name__ == '__main__':

    w = 31
    N, e, d, p, q = GenRSA("0"*w)

    d_bin = "{0:b}".format(d)
    d_start, k = find_prefix(N, d)
    c_to_r = dict()
    cs = []
    for c in generate_c(N, w):
        x, h, r = fast_pow(c, N, d)
        x, h, r2 = fast_pow(c, N, d_start)
        c_to_r[c] = r - r2

        cs.append(c)


    c_c_exp_list = [(c, dec(c, N, d_start)) for c in cs]

    d_prefix = d_start
    print("{0:b}".format(d))
    print("{0:b}".format(d_prefix), end='')
    for _ in range(w - k + 2):
        reduced_rs = []
        n_reduced_rs = []
        for c, c_exp in c_c_exp_list:
            if is_reduced(c, c_exp, N):
                reduced_rs.append(c_to_r[c])
            else:
                n_reduced_rs.append(c_to_r[c])

        while len(n_reduced_rs) == 0 or len(reduced_rs) == 0:
            c = next(c_generator)

            x, h, r = fast_pow(c, N, d)
            c_exp, h, r2 = fast_pow(c, N, d_prefix)
            c_to_r[c] = r - r2

            cs.append(c)
            if is_reduced(c, c_exp, N):
                reduced_rs.append(c_to_r[c])
            else:
                n_reduced_rs.append(c_to_r[c])
            c_c_exp_list.append((c, c_exp))



        reduced_mean = np.mean(reduced_rs)
        n_reduced_mean = np.mean(n_reduced_rs)


        d_prefix *= 2
        if n_reduced_mean < reduced_mean - 0.2:
            d_prefix += 1
            print('1', end='')
            for c, c_exp in c_c_exp_list:
                c_exp = c_exp*c_exp
                if c_exp >= N:
                    c_exp %= N
                    c_to_r[c] -= 1
                c_exp *= c
                if c_exp >= N:
                    c_exp %= N
                    c_to_r[c] -= 1
            c_c_exp_list = [(c, c *c_exp * c_exp % N) for c, c_exp in c_c_exp_list]
        else:
            print('0', end='')
            for c, c_exp in c_c_exp_list:
                c_exp_sq = c_exp*c_exp
                if c_exp_sq >= N:
                    c_exp_sq %= N
                    c_to_r[c] -= 1
            c_c_exp_list= [(c, c_exp*c_exp % N) for c, c_exp in c_c_exp_list]

    c = 5
    while  enc(dec(c,N,d_prefix), N, e) != c:
        d_prefix //= 2
        if d_prefix == 0:
            print()
            print("solution not found")
            exit(1)
    print()
    print("final solution")
    print("{0:b}".format(d_prefix))
    print("key")
    print("{0:b}".format(d))