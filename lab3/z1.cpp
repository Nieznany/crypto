#include <gmpxx.h>

#include <iostream>
#include <vector>

constexpr int n = 256;
int main() {
    // GENEROWANIE
    gmp_randclass random (gmp_randinit_default);
    mpz_class sum = 0;
    std::vector<mpz_class> w(n);
    int z = 40;
    for(int i = 0; i < n; ++i) {
        w[i] = sum + random.get_z_bits(z);
        sum += w[i];
        std::cout << "w_" << i << " = " << w[i] << std::endl;
    }
    mpz_class q = sum + random.get_z_bits(z);
    std::cout << "q = " << q << std::endl;
    
    
    mpz_class r;
    do {
        r = random.get_z_range(q);
    } while(gcd(r, q) != 1);
    std::cout << "r = " << r << std::endl;
    mpz_class r_prim; 
    mpz_invert(r_prim.get_mpz_t(), r.get_mpz_t(), q.get_mpz_t());
    std::cout << "r' = " << r_prim << std::endl;
    std::cout << "r'*r = " << r_prim*r % q << std::endl;
    
    std::vector<mpz_class> b(n);
    for(int i = 0; i < n; ++i) {
        b[i] = w[i] * r % q;
        std::cout << "b_" << i << " = " << b[i] << std::endl;
    }
    // Szyfrowanie
    mpz_class m = random.get_z_bits(n);
    std::cout << "m = " << m << std::endl;
    
    mpz_class c = 0;
    for(int i = 0; i < n; ++i) {
        c += (m % 2) * b[n - i - 1];
        m /= 2;
    }
    std::cout << "c = " << c << std::endl;
    // DESZYFROWANIE
    mpz_class c_prim = c*r_prim % q;
    std::cout << "c' = " << c_prim << std::endl;

    mpz_class m_prim = 0;
    std::vector<mpz_class> x(n);
    for(int i = 0; i < n; ++i) {
        if(w[n - i - 1] <= c_prim) {
            c_prim -= w[n - i - 1];
            x[n - i - 1] = 1;
        }
        else {
            x[n - i - 1] = 0;
        }
    }
    for(int i = 0; i < n; ++i) {
        m_prim *= 2;
        m_prim += x[i];
    }
    std::cout << "m' = "<< m_prim << std::endl;
    
    // SHAMIR ATTACK
    
    // USING FOUND "KEY"
    
    return 0;
}
