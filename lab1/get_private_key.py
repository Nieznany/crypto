#!/usr/bin/python3

import sys
import sympy

from Crypto.PublicKey import RSA

def main():
    p = int(sys.argv[1])
    q = int(sys.argv[2])
    n = p*q
    e = int(sys.argv[3])
    d = modular_inverse = sympy.mod_inverse(e, (p-1)*(q-1))

    print(f"n={n}")
    print(f"d={d}")
    
    key = RSA.construct((n, e, d, p, q))
    text_key = key.exportKey()
    
    with open("cakey.pem", "wb") as opened_file:
        opened_file.write(text_key)
    


if __name__ == "__main__":
    main()
