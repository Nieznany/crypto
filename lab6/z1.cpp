#include <gmpxx.h>
#include <iostream>
#include <utility>

gmp_randclass rng(gmp_randinit_default);

class EdwardsCurve;

class ModuloField {
public:
    ModuloField(mpz_class value, mpz_class modulo) : 
        value(value), modulo(modulo) 
    {
        while (value < 0) {
            value += modulo;
        }
        value %= modulo;
    }
    bool operator==(const ModuloField& other) const{
        return value == other.value;
    }
    
    ModuloField operator+(const ModuloField& other) const{
        return ModuloField((value + other.value) % modulo, modulo);
    }
    
    ModuloField operator-(const ModuloField& other) const{
        return ModuloField((value - other.value + modulo) % modulo, modulo);
    }
    ModuloField operator-() const{
        return ModuloField((modulo - value) % modulo, modulo);
    }
    
    ModuloField operator*(const ModuloField& other) const{
        return ModuloField((value * other.value) % modulo, modulo);
    }
    
    ModuloField operator/(const ModuloField& other) const{
        mpz_class res;
        mpz_invert(res.get_mpz_t(), other.value.get_mpz_t(), modulo.get_mpz_t());
        return ModuloField((value * res) % modulo, modulo);
    }
    
    mpz_class getModulo() const {
        return modulo;
    }
private:
    mpz_class value;
    mpz_class modulo;
    
    friend std::ostream &operator<<(std::ostream &os, const ModuloField &m);
};

class Point {
public:
    Point(const ModuloField& x, const ModuloField& y, const ModuloField& d) : x(x), y(y), d(d) {}
    bool operator==(const Point& other) const {
        return x == other.x && y == other.y;
    }
    
    bool onCurve() const {
        auto modulo = x.getModulo();
        return x*x + y*y == ModuloField(1, modulo) + d*x*x*y*y;
    }
    
    Point operator+(const Point& other) const {
        auto modulo = x.getModulo();
        ModuloField one(1, modulo);
        ModuloField x1 = this->x;
        ModuloField y1 = this->y;
        ModuloField x2 = other.x;
        ModuloField y2 = other.y;
        ModuloField x3 = (x1*y2+y1*x2)/(one+d*x1*y1*x2*y2);
        ModuloField y3 = (y1*y2-x1*x2)/(one-d*x1*y1*x2*y2);
        return Point(x3, y3, d);
    }
    
    Point operator-(const Point& other) const {
        auto modulo = x.getModulo();
        ModuloField one(1, modulo);
        ModuloField x1 = this->x;
        ModuloField y1 = this->y;
        ModuloField x2 = -other.x;
        ModuloField y2 = other.y;
        ModuloField x3 = (x1*y2+y1*x2)/(one+d*x1*y1*x2*y2);
        ModuloField y3 = (y1*y2-x1*x2)/(one-d*x1*y1*x2*y2);
        return Point(x3, y3, d);
    }
    
    Point operator*(mpz_class exp) const {
        auto modulo = x.getModulo();
        Point r0(ModuloField(0, modulo), ModuloField(1, modulo), d);
        Point r1 = *this;
        size_t m = mpz_sizeinbase(exp.get_mpz_t(), 2);
        for(size_t i = 0; i < m; ++i)
        {
            int bit = mpz_tstbit(exp.get_mpz_t(), m - i - 1);
            if (bit == 1) {
                r0 = r0 + r1;
                r1 = r1 + r1;
            }
            else {
                r1 = r0 + r1;
                r0 = r0 + r0;
            }
        }
        return r0;
    }
private:
    ModuloField d;
    ModuloField x;
    ModuloField y;
    
    friend std::ostream &operator<<(std::ostream &os, const Point &m);
};

class EdwardsCurve {
public:
    EdwardsCurve(mpz_class modulo, mpz_class d, mpz_class p) : modulo(modulo), d(d, modulo), p(p) {
        
    }
    
    Point getBasePoint() {
        mpz_class x("2705691079882681090389589001251962954446177367541711474502428610129");
        mpz_class y = 28;
        return Point(ModuloField(x, modulo), ModuloField(y, modulo), d);
    }
    
    Point getZeroPoint() {
        mpz_class x = 0;
        mpz_class y = 1;
        return Point(ModuloField(x, modulo), ModuloField(y, modulo), d);
    }
    
    std::pair<mpz_class, Point> getKey() {
        mpz_class k = 0;
        while (k <= 1)
            k = rng.get_z_range(p);
        k = k * 4;
        Point beta = getBasePoint() * k;
        return {k, beta};
    }
    
    std::pair<Point, Point> encrypt(Point m, const Point& beta) {
        Point alpha = getBasePoint();
        mpz_class x = 0;
        while (x <= 1)
            x = rng.get_z_range(p);
        return {alpha * x, m + beta * x};
    }
    
    Point decrypt(std::pair<Point, Point> ciphertext, mpz_class k) {
        auto [alpha_x, m_beta_x] = ciphertext;
        if (!alpha_x.onCurve()) {
            throw "not on curve";
        }
        Point beta_x = alpha_x * k;
        if (beta_x == getZeroPoint()) {
            throw "Special point";
        }
        
        
        return m_beta_x - beta_x;
    }
 
private:
    mpz_class modulo;
    mpz_class p;
    ModuloField d;
};

std::ostream &operator<<(std::ostream &os, const ModuloField &m) { 
    return os << m.value;
}

std::ostream &operator<<(std::ostream &os, const Point &m) { 
    return os << m.x << " " << m.y;
}

std::ostream &operator<<(std::ostream &os, const std::pair<Point, Point> &m) { 
    return os << m.first << " " << m.second;
}


int main() {
    // Curve E-222
    mpz_class fp = 1;
    fp <<= 222;
    fp -= 117;
    mpz_class p("1684996666696914987166688442938726735569737456760058294185521417407");
    
    mpz_class d = 160102;
    EdwardsCurve curve(fp, d, p);
    
    Point alpha = curve.getBasePoint();
    auto [k, beta] = curve.getKey();
    
    std::cout << "alpha: " << alpha << std::endl;
    std::cout << "beta: " << beta << std::endl;
    
    mpz_class m_int = 1234123;
    Point m = curve.getBasePoint() * m_int;
    std::cout << "m:      " << m << std::endl;
    auto c = curve.encrypt(m, beta);
    Point e_m = curve.decrypt(c, k);
    std::cout << "Dec(c): " << e_m << std::endl;
    std::cout << "c: " << c << std::endl;
    
}
