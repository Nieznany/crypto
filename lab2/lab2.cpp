#include <list>
#include <vector>
#include <stdexcept>
#include <iostream>
#include <cstring>

#include <openssl/rand.h>
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>

using namespace std;

int encrypt(unsigned char *plaintext, int plaintext_len, unsigned char *key,
            unsigned char *iv, unsigned char *ciphertext)
{
    EVP_CIPHER_CTX *ctx;
    int len;
    int ciphertext_len = 0;
    if(!(ctx = EVP_CIPHER_CTX_new()))
    {
        throw std::runtime_error("Błąd poczas kodowania.");
    }
    if(1 != EVP_EncryptInit_ex(ctx, EVP_aes_128_cbc(), NULL, key, iv))
    {
        throw std::runtime_error("Błąd poczas kodowania.");
    }
    if(1 != (EVP_CIPHER_CTX_set_padding(ctx, 0)))
    {
        throw std::runtime_error("Błąd poczas kodowania.");
    }
    if(1 != EVP_EncryptUpdate(ctx, ciphertext, &len, plaintext, plaintext_len))
    {
        throw std::runtime_error("Błąd poczas kodowania.");
    }
    ciphertext_len += len;
    if(1 != EVP_EncryptFinal_ex(ctx, ciphertext + len, &len))
    {
        throw std::runtime_error("Błąd poczas kodowania.");
    }
    ciphertext_len += len;
    
    EVP_CIPHER_CTX_free(ctx);
    return ciphertext_len;
}

int decrypt(unsigned char *ciphertext, int ciphertext_len, unsigned char *key,
            unsigned char *iv, unsigned char *plaintext)
{
    EVP_CIPHER_CTX *ctx;
    int len;
    int plaintext_len = 0;
    if(!(ctx = EVP_CIPHER_CTX_new())) {
        throw std::runtime_error("Błąd poczas dekodowania.");
    }
    if(1 != EVP_DecryptInit_ex(ctx, EVP_aes_128_cbc(), NULL, key, iv)) {
        throw std::runtime_error("Błąd poczas dekodowania.");
    }
    if(1 != (EVP_CIPHER_CTX_set_padding(ctx, 0))) {
        throw std::runtime_error("Błąd poczas dekodowania.");
    }
    if(1 != EVP_DecryptUpdate(ctx, plaintext, &len, ciphertext, ciphertext_len)) {
        throw std::runtime_error("Błąd poczas dekodowania.");
    }
    plaintext_len += len;
    if(1 != EVP_DecryptFinal_ex(ctx, plaintext + len, &len)) {
        throw std::runtime_error("Błąd poczas dekodowania.");
    }
    plaintext_len += len;

    EVP_CIPHER_CTX_free(ctx);
    return plaintext_len;
}

class Puzzle {
private:
    unsigned long long size;
    unsigned int n;
    unsigned char constant[16];
    unsigned char iv[16];
    unsigned char key_base[16];
    // Ta lista powoduje złożoność pamięciową O(2^n) z tego powodu dla n=32,
    // nie jesteśmy w stanie wygenerować wszystkich puzzli (na moim laptopie).
    // Można zredukować złożoność pamięciową do O(1)
    // generując przy każdym przesłaniu na nowo puzzle (gdzie ziarno PRNG jest ustalone), a Bob wtedy by zapamiętał tylko wybrany element
    std::list<vector<unsigned char>> puzzles;
    
    void clean_key_base() {
        for(int i = 0; i < sizeof size; ++i) {
            key_base[i] = 0;
        }
    }
    
public:
    Puzzle(unsigned int n, unsigned char* key) : n(n), size(1ull << n){
        if (n % 8) {
            throw std::runtime_error("n musi być wielokrotnością 8 (z lenistwa programisty)");
        }
        unsigned char temp_key[16];
        if (!RAND_bytes(temp_key, sizeof temp_key)) {
            throw std::runtime_error("Błąd poczas losowana.");
        }
        if (!RAND_bytes(constant, sizeof constant)) {
            throw std::runtime_error("Błąd poczas losowana.");
        }
        if (!RAND_bytes(iv, sizeof iv)) {
            throw std::runtime_error("Błąd poczas losowana.");
        }
        if (!RAND_bytes(key_base, sizeof key_base)) {
            throw std::runtime_error("Błąd poczas losowana.");
        }
        unsigned char padded_i[16] = {};
        unsigned char encoded_puzzle[16*3];
        std::memcpy(encoded_puzzle + 16*2, constant, 16);
        for(unsigned long long i = 0; i < size; ++i) {
            // Pętla wykonuje się w czasie O(2^n)
            *(unsigned long long*)padded_i = i;
            unsigned char id[16];
            unsigned char id_key[16];
            if (sizeof id != encrypt(padded_i, sizeof padded_i, temp_key, iv, id)) {
                throw std::runtime_error("Błąd poczas enkrypcji id.");
            }
            if (sizeof id_key != encrypt(id, sizeof id, key, iv, id_key)) {
                throw std::runtime_error("Błąd poczas enkrypcji klucza.");
            }
            if (!RAND_bytes(key_base, sizeof size)) {
                throw std::runtime_error("Błąd poczas losowana.");
            }
            *(unsigned long long*)key_base &= size - 1ull;
            std::memcpy(encoded_puzzle, id, 16);
            std::memcpy(encoded_puzzle + 16, id_key, 16);
            puzzles.emplace_back(48);
            if (48 != encrypt(encoded_puzzle, 48, key_base, iv, &puzzles.back().front())) {
                throw std::runtime_error("Błąd poczas enkrypcji puzzla.");
            }
        }
        clean_key_base();
    }
    
    unsigned long long get_size() {
        return size;
    } 
    
    void get_encoded_puzzle(unsigned char* dst, unsigned long long i) {
        unsigned char temp[16*3];
        auto it = puzzles.begin();
        std::advance(it, i);
        for(unsigned long long temp_key = 0; temp_key < size; ++temp_key) {
            // znowy mamy n^2 iteracji pętli, która wykonuje się w czasie stałym
            *(unsigned long long*) key_base = temp_key;
            if (48 != decrypt(&(it->front()), 48, key_base, iv, temp)) {
                throw std::runtime_error("Błąd poczas dekrypcji puzzla.");
            }
            if (memcmp(constant, temp+16*2, 16) == 0) {
                memcpy(dst, temp, sizeof temp);
                return;
            }
        }
        throw std::runtime_error("Błąd poczas szukania id.");
    }
    
    void get_key(unsigned char* dst, unsigned char* priv_key, unsigned char* id) {
        if (16 != encrypt(id, 16, priv_key, iv, dst)) {
            throw std::runtime_error("Błąd poczas enkrypcji klucza.");
        }
    }
};

// Złożoność Alicji (2^n, 2^n),
// Złożoność Boba (2^n, 2^n), 

int main() {
    unsigned char priv_key[16];
    std::cout << std::hex;
    std::cout << "Alicja: Generowanie prywatnego klucza" << std::endl;
    if (!RAND_bytes(priv_key, sizeof priv_key)) {
        throw std::runtime_error("Błąd poczas losowana.");
    }
    std::cout << "Alicja: Generowanie puzzle" << std::endl;
    Puzzle puzzle(24, priv_key);
    
    std::cout << "przekazania puzzle: Alicja -> Bob" << std::endl;
    
    std::cout << "Bob: losowanie i" << std::endl;
    unsigned long long i;
    if (!RAND_bytes((unsigned char*)&i, sizeof i)) {
        throw std::runtime_error("Błąd poczas losowana.");
    }
    i &= puzzle.get_size() - 1ull;
    
    std::cout << "Bob: Łamanie puzzle" << std::endl;
    unsigned char encoded_puzzle[16*3];
    puzzle.get_encoded_puzzle(encoded_puzzle, i);
    unsigned char* id = encoded_puzzle;
    unsigned char* bob_key = encoded_puzzle + 16;
    std::cout << "Bob: otrzymany klucz: ";
    for (int i = 0; i < 16; ++i) {
        std::cout << (int)bob_key[i];
    }
    std::cout << std::endl;
    std::cout << "przekazania id: Bob -> Alicja" << std::endl;
    std::cout << "Alicja: liczenie klucza z id." << std::endl;
    unsigned char alice_key[16];
    puzzle.get_key(alice_key, priv_key, id);
    std::cout << "Alicja: otrzymany klucz: ";
    for (int i = 0; i < 16; ++i) {
        std::cout << (int)alice_key[i];
    }
    std::cout << std::endl;
    return 0;
}
